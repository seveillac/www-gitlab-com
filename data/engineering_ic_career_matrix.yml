- name: Intermediate
  "Values Alignment":
    Collaboration:
    - Helps their team succeed and regularly engages with stable-counterparts.
    - Gives positive feedback in public and negative feedback privately with care.
    Results:
    - Demonstrates ownership of team projects, showing bias for action to deliver projects on time.
    Efficiency:
    - Finds and implements simple solutions that reduce the complexity of the team's product or the team's processes.
    - Shows examples of accomplishing more with less in a frugal manner.
    Diversity:
    Iteration:
    - Demonstrates understanding of the balance between short term gains and long term benefit.
    - Focuses on improvement.
    Transparency:
    - Consistently provides context and background on projects and issues so that those with no prior knowledge are able to contribute to the discussion.
  "Technical Competencies":
    Quality:
    - Expected to write tests with every MR and work to uplift the quality of the code.
    - When working in an existing area of the code base, strives to leave it in a better state than before.
    Security:
    - Able to take on security issues identified by the security team and provide a solution.
    Complexity:
    - Able to take on large issues with well defined and clear requirements and design implementation.
    - Asks good questions to clarify expectation.
    - Recognizes when an issue becomes much larger than originally expected so the work can be discussed and split into further issues.
    "Technical Stewardship":
    - A good steward of the team's code.
    - Perform thorough reviews within your domain and submit helpful comments.
    Performance:
    - Able to resolve moderately complex performance issues independently.
    - Able to monitor and debug performance issues of moderate complexity and resolve them.
    "Open Source":
    - Encourages participation from those in the community.
  "Leadership Competencies":
    "Growing others":
    - Often works with other team members to grow their own understanding of the team's domain through code reviews, pair programming or mentoring sessions.
    - Participates in code reviews and uses [best practices](https://docs.gitlab.com/ee/development/code_review.html#best-practices) when giving and receiving feedback.
    - Provides additional feedback whenever possible (i.e. 360s, Pulse, etc).
    - Continually looks for opportunities to grow their technical skills
    "Communication":
    - Communicates clearly and effectively, and follows the GitLab [communication guidelines](https://about.gitlab.com/handbook/communication/).
    "Sphere of Influence":
    - Produces work that impacts an their group.
    "Ambiguity":
    - Delivers work given clear requirements within the context of their team.
    - Makes responsible decisions, and evaluates trade-offs.
    - Remains calm under pressure.
    "Business Acumen":
    - Demonstrates knowledge about their features and caregory and how it fits into their group.
    "Process":
    - Works effectively within established processes, and keeps themselves current as processes change.
    - Seeks to understand how their works integrates with processes across the company

- name: Senior
  "Values Alignment":
    Collaboration:
    - Helps their team succeed. Provides feedback that focuses on the business impact of the behaviour.
    - Extensive user of GitLab (dogfooding) and regularly makes helpful suggestions for feature proposals.
    Results:
    - Demonstrates ownership of projects that require collaboration with other teams in their sub department.
    - Creates a sense of urgency to deliver results and encourages bias for action in others.
    Efficiency:
    - Chooses work that enables greater efficiency of others in the team.
    Diversity:
    - Seeks out diverse opinions to improve the quality of the team's work.
    Iteration:
    - Creates proposals for team work that clearly defines the problem, offers alternatives, and proposes a solution.
    - Identifies opportunities for the team to deliver results in a more iterative way.
    Transparency:
    - Holds their team to a higher standard of transparency and encourages others to work in the open.
  "Technical Competencies":
    Quality:
    - It is important that at every level we adhere to the minimum requirement of writing tests with every MR.
    - Expected to uplevel the quality of the code.
    - When working in an existing area, strives to leave it in a better state than before.
    - Improves the state of our test framework and any other dependencies.
    - Work to guide others and improve the performance of the tests.
    - Always looks to improve our test coverage with every MR coded or reviewed.
    Security:
    - In addition to fixing security issues, contribute to the team's understanding of security and how they impact the team's domain.
    - Able to take on issues, come up with an effective approach and deliver a working solution.
    Complexity:
    - Able to take on large issues with vague requirements and design implementation.
    - Asks good questions to clarify expectation.
    - Recognizes when an issue becomes much larger than originally expected and able to create further issues and propose the next simplest implementation to follow.
    "Technical Stewardship":
    - Care for GitLab's code base.
    - Provide helpful reviews across many domains and be mindful of how changes may impact other teams.
    - Be a Maintainer for smaller projects.
    Performance:
    - Able to resolve complex performance issues and mentor others on best practices.
    - Understands how code performs in production on GitLab.com and impact of the architecture and dependencies required for the self-hosted product
    "Open Source":
    - Act as coach for open source contributors.
    - Encourage participation from those in the community.
  "Leadership Competencies":
    "Growing others":
    "Communication":
    - Communicates clearly and effectively, and follows the GitLab [communication guidelines](https://about.gitlab.com/handbook/communication/).
    "Sphere of Influence":
    - Produces work that impacts the entire team, and potentially other teams
    "Ambiguity":
    - Delivers work given unclear requirements within the context of their team.
    - Makes responsible decisions, and evaluates trade-offs.
    - Maintains calm under pressure.
    "Business Acumen":
    - Demonstrates knowledge about their groups, and how their stage fits into the larger business.
    - Also exhibits an understanding of a number of other team's products
    "Process":
    - Seeks to understand how their work integrates with processes across the company.
    - Looks for opportunities for process improvements within their team and works with others to implement process changes