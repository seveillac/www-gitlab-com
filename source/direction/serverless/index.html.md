---
layout: markdown_page
title: "Product Vision - Serverless"
---

- TOC
{:toc}

Serverless computing provides an easy way to build highly scalable applications and services, eliminating the pains of provisioning & maintaining.

Today it's mostly used one-off for e.g. image transformations and ETL, but given the potential and the rise of microservices, it's fully possible to build complex, complete applications on nothing but serverless functions and connected services. Leveraging knative and kubernetes, users will be able to define and manage functions in GitLab. This includes security, logging, scaling, and costs of their serverless implementation for a particular project/group.

## What's next & why
- Deploy serverless apps with [gitlabktl](https://gitlab.com/gitlab-org/gitlabktl/issues/9)

Now that we've built gitlabktl (a CLI that allows us to build and deploy Knative apps using GitLab CI) we want to use it for both serverless functions (faas) as well as serverless applications. This will allow users to take full advantage of all the great features of GitLab CI when deploying serverless workloads to Knative.

## North Stars

We apply our global product strategy to thinking about the serverless space. We treat the following
principles as our north stars:

## Stages with serverless focus

There are several stages involved in delivering a comprehensive, quality serverless experience at GitLab. These include, but are not necessarily limited to the following:

- [Configure](/direction/configure/)

## Highlighted epics and issues

There are a few epics and important issues you can check out to see where we're headed.

- [&155](https://gitlab.com/groups/gitlab-org/-/epics/155): Improve mobile CI/CD in GitLab
